//+------------------------------------------------------------------+
//|                                                 CORE__RIPPER.mq4 |
//|                        Copyright 2022, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2022, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict



#define GET_HISTORY "GET_HISTORY"
#define API_URL "http://2286498.td430236.web.hosting-test.net"

#include "CORE__HELPERS.mqh"


int periods[] = {
   1,
      15, 60,240,1440
   };
      
string symbols[] = {
   "XAUUSD",
   "GBPUSD",
   "EURUSD",
   "AUDUSD",
   "NZDUSD",
   "USDCAD",
   "GBPJPY",
   "EURJPY",
   "AUDJPY",
   "USDJPY",
   "GBPCHF",
   "BTCUSD",
};

Seeder *seed = NULL;
Hash *env = NULL;   
CJAVal *state;




int OnInit()
{
   EventSetTimer(1);
   state = new CJAVal();
   init_state(state);


   
   seed = new Seeder(state);   
   seed.export_qqe_timeseria(periods, symbols, 0);

   //seed.export_qqe_crosses(periods, symbols);
   commands_loop(periods, symbols);
   

   //log("first line", "");
   //log("second line", "");
   

   Print("RIPPER STARTED");
   log("RIPPER STARTED", "");

   return(INIT_SUCCEEDED);
}

   void OnDeinit(const int reason)
  {
      EventKillTimer();
         
         delete seed;
         delete state;
  
  }
  


  void OnTimer()
  {

      //seed.export_qqe_crosses(periods, symbols);
      //seed.export_qqe_timeseria(periods, symbols, 0);

      //commands_loop(periods, symbols);
  }

  
/**
 * ============================================================================================
 * ============================================================================================
 * ============================================================================================
 * ============================================================================================
 * ============================================================================================
 * ============================================================================================
 * 
 * */
  
  void init_state(CJAVal &s)
  {
      s["is_qqe_seeding"]=0;
      s["is_qqe_cross_seeding"]=0;

      //s["symbols"] = StringConcatenate(symbols);

  }
int commands_loop(int &periods[], string &symbols[])
{

      CJAVal data = http_get(url__api_url("/forex/commands/sheet"));
      CJAVal *tasks = data.FindKey("list");
      
      
      for (int j=0;j < data["count"].ToInt(); j++)
      {
         CJAVal *task = tasks[j];
         CJAVal *params = task.FindKey("params");

         log("commands_loop task :", task.Serialize());


         CJAVal result;
         switch(task["uniq_id"].ToInt())
         {
            case 0: 
               result = STATUS_handler(params);
               break;
            case 1: 
               result = GET_HISTORY_TIMESERIA_handler(params, periods, symbols);
               break;
            // case 2: 
            //    result = GET_OPEN_ORDERS_handler(params);
            //    break;
            // case 3: 
            //    result = OPEN_MARKET_ORDER_handler(params);
            //    break;
            // case 4: 
            //    result = CLOSE_ORDER_handler(params);
            //    break;
               
            default:
               log("UNKONWN TASK ID" + task["uniq_id"].ToInt(), "");

         }


          result["processed_at"] = TimeToStr(TimeCurrent());

          send_command_result(task["id"].ToInt(), task["name"].ToStr(), result.Serialize());
      }
   
      return 1;
}

CJAVal GET_OPEN_ORDERS_handler(CJAVal *params)
{
   CJAVal ret;


   return ret;
}


CJAVal STATUS_handler(CJAVal *params)
{
  
   CJAVal ret;

   ret["account_company"] = AccountCompany(); 
   ret["account_server"] = AccountServer();
   ret["account_name"] = AccountName(); 
   ret["account_number"] = AccountNumber(); 
   ret["account_balance"]=AccountBalance(); 
   ret["account_leverage"] = AccountLeverage();
   
   
   ret["state"].Add(state);

   return ret;
}



CJAVal GET_HISTORY_TIMESERIA_handler(CJAVal *params, int &periods[], string &symbols[])
{
   CJAVal ret;

   if(params["name"].ToStr() == "qqe")
   {
      seed.export_qqe_timeseria(periods, symbols, 100);

   } else if(params["name"].ToStr() == "qqe2")
   {

   } else {
      log("GET_HISTORY_TIMESERIA_handler", "No such HISTORY_TIMESERIA:" + params["name"].ToStr());
   }
   

   
   return ret;

}
