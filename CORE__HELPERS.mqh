//+------------------------------------------------------------------+
//|                                                 core_helpers.mqh |
//|                        Copyright 2022, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2022, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#include "hash.mqh"
//#include "json.mqh"
#include "jAson.mqh"

//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
// #define MacrosHello   "Hello, world!"
// #define MacrosYear    2010



void log(string header, string line)
{
   Hash *data = new Hash();
   data.hPutString("_params", "header,line");
   data.hPutString("header", header);
   data.hPutString("line", line);
   data.hPutString("processed_at", TimeToStr(TimeCurrent()));

   http_post(url__api_url("/forex/log"), data);
}


void set_flag(string prefix, string flag, int value)
{
   GlobalVariableSet(prefix + "-" + flag, value);
}

bool check_flag(string prefix, string flag, int value)
{
   return GlobalVariableGet(prefix + "-" + flag) == value;
}



int send_command_result(int command_id, string command, string result)
{
   Hash *data = new Hash();
   data.hPutString("_params", "command_id,command_name,result");
   data.hPutString("command_id", command_id);
   data.hPutString("command_name", command);
   data.hPutString("result", result);


   http_post(url__api_url("/forex/commands/webhook"), data);
   return 0;
}


CJAVal http_get(string url)
{
   string cookie=NULL,headers;
   char post[],result[];
   int res;
   
   ResetLastError();
   res=WebRequest("GET",url,cookie,NULL,5000,post,0,result,headers);   

   Print("HTTP GET :", url);
   Print("HTTP GET RESULT :", CharArrayToString(result));

   //log("HTTP GET :" + url);
   //log(CharArrayToString(result));

   if(res==-1)
   {
      Print("Error in WebRequest.", " Error code  =" + GetLastError());
   }

   CJAVal json;
   json.Deserialize(CharArrayToString(result));

   return json.FindKey("data");
}

string http_post(string url, Hash *data)
{
      string cookie=NULL,headers;
      char post[],result[];
      int res;
      string str= "";

      string params[];
      StringSplit(data.hGetString("_params"), ',' ,params);

      for(int i=0;i<ArraySize(params);i++)
      {
         str += params[i] + "=" + data.hGetString(params[i]) + "&";
      }
      str = StringSubstr(str, 0, StringLen(str)-1);

      //string str="data="+postData+"&symbol="+symbol+"&period="+period+"&tag="+tag;
      ArrayResize(post,StringToCharArray(str,post,0,WHOLE_ARRAY,CP_UTF8)-1);
      ResetLastError();
            
      //--- Loading a html page from Google Finance
      int timeout=5000; //--- Timeout below 1000 (1 sec.) is not enough for slow Internet connection
      res=WebRequest("POST", url,cookie,NULL,timeout,post,0,result,headers);
   //--- Checking errors
      
      Print("HTTP_POST: " + url, " || " + str);
      Print("HTTP_POST RESULT: " + CharArrayToString(result));

      if(res==-1)
      {
         Print("Error in WebRequest. Error code  ="+GetLastError(), "");
      }
      
      return "";
}

string url__api_url(string url)
{
   return API_URL + url;
}


/**
 Seeder for qqe timeseria and signals
*/
class Seeder {
    public:
        CJAVal *_state;


        Seeder(CJAVal *state) {
            _state = state;      
        }

        ~Seeder() {
               delete _state;
        }


      int export_qqe_timeseria(int &periods[], string &symbols[], int history)
      {
         
            _state["qqe_last_time_processed"] = TimeToStr(TimeCurrent());
            

            for (int j=0;j < ArraySize(symbols); j++)
           {
               string symbol = symbols[j];
               CJAVal ret;

               for (int i=0;i < ArraySize(periods);i++)
               {
                  
                  int period = periods[i];
                  string postData = "";
                  
                  for (int i=history;i >=0;i--)
                  {
                        //Print(symbol, " ", period, " ",  i, iTime(symbol, period, i));

                        double val=iCustom(symbol, period, "QQE_ADV", 0, i); // 0 - fast, 1 - slow
                        postData = StringConcatenate(postData, StringConcatenate(iTime(symbol, period, i), ",", val), ";");
                  }

                  CJAVal temp;
                  temp[(string)period] = postData;
                  ret[symbol].Add(temp);
               }

               Hash *p = new Hash();
               p.hPutString("_params", "timeseries,tag");
               p.hPutString("timeseries", ret.Serialize());
               p.hPutString("tag", "qqe");
               http_post(url__api_url("/timeseries?qqe"), p);

           }
     
           
           return 0;
      }


      int export_qqe_crosses(int &periods[], string &symbols[])
      {

         _state["qqe_crosses_last_time_processed"] = TimeToStr(TimeCurrent());
         
         for (int j=0;j < ArraySize(symbols); j++)
           {
               string symbol = symbols[j];
               
               for (int i=0;i < ArraySize(periods);i++)
               {
                  int period = periods[i];
                  string postData = "";
                                 
                  double fast_prev=iCustom(symbol, period, "QQE_ADV", 2, 1); // 2 - fast one    0 - slow  
                  double fast=iCustom(symbol, period, "QQE_ADV", 2, 0); // 2 - fast one    0 - slow
                  double slow=iCustom(symbol, period, "QQE_ADV", 0, 0); // 2 - fast one    0 - slow
                  
                  //Print(symbol, period,"  ",  fast_prev,"  ", slow,"  ", fast);

                  
                  if(fast_prev > slow && slow > fast) {

                     if(!check_flag("qqe_cross", symbol + "-=" + period, 0))
                     {
                           set_flag("qqe_cross", symbol + "-=" + period, 0);
                           
                           string url= url__api_url("/signal_history") + "?tag=qqe_cross&symbol="+symbol+"&period="+period+"&direction=0";
                           http_get(url);
                     }                        
                     
                  }
                  
                  if(fast_prev < slow && slow < fast) {
                     if(!check_flag("qqe_cross", symbol + "-=" + period, 1))
                     {
                        set_flag("qqe_cross", symbol + "-=" + period, 1);
                           
                        string url= url__api_url("/signal_history") + "?tag=qqe_cross&symbol="+symbol+"&period="+period+"&direction=1";
                        http_get(url);
                     }
                  }
                  
               
               
               }
            
           }
         return 0;
      }
};